<?php

define('ANONYMOUS_USER_ID', -1);

// TODO: Is this the best way to do this? I lose the IntelliSense for queries, and it looks ugly
define('TABLE_EVENT_HANDLERS', config('database', 'table_prefix') . 'event_handlers');
define('TABLE_GROUP_MEMBERS', config('database', 'table_prefix') . 'group_members');
define('TABLE_GROUP_PERMISSIONS', config('database', 'table_prefix') . 'group_permissions');
define('TABLE_GROUPS', config('database', 'table_prefix') . 'groups');
define('TABLE_PATHS', config('database', 'table_prefix') . 'paths');
define('TABLE_MODULES', config('database', 'table_prefix') . 'modules');
define('TABLE_SESSIONS', config('database', 'table_prefix') . 'sessions');
define('TABLE_USERS', config('database', 'table_prefix') . 'users');

// TODO: Decide how to handle module constants