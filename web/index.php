<?php

// Load Composer-installed packages
require('../vendor/autoload.php');

// Use custom error handler (Done after autoload because that also loads all classes)
set_error_handler(['\JAF\Error', 'errorHandler']);
register_shutdown_function(['\JAF\Error', 'phpErrorHandler']); // For fatal errors
ini_set('display_errors', false);

require('../common.php');

$jaf = new \JAF\Jaf();

/**
 * Make the core globally accessible
 *
 * @return \JAF\Jaf
 */
function jaf()
{
	global $jaf;
	return $jaf;
}

$jaf->execute();