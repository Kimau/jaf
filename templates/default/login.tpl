<div class="box">
    <form id="login" method="post" action="">
        <fieldset>
            <div>
                <label for="username">{ln}USERNAME{/ln}</label>
                <input id="username" name="username" type="text" required autofocus>
            </div>
            <div>
                <label for="password">{ln}PASSWORD{/ln}</label>
                <input id="password" name="password" type="password" placeholder="********" required>
            </div>
            <div>
                <label for="long_session">{ln}LONG_SESSION{/ln}</label>
                <input id="long_session" name="long_session" type="checkbox" value="1">
            </div>
            <div>
                <input name="login" type="submit" value="{ln}LOG_IN{/ln}">
            </div>
        </fieldset>
    </form>
</div>
