<html>
<head>
    <title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="/styles/{$template}.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
		var active_module = "{$active_module}";
		var messages = JSON.parse('{$messages}');
    </script>
    <script type="text/javascript" src="/scripts/_jaf.js"></script>
    <script type="text/javascript" src="/scripts/{$template}.js"></script>
</head>
<body>
<div class="document-container">