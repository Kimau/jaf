(function (jaf, $, undefined) {

	// Displays messages given from system
	$(function () {
		for (let i = 0; i < messages.length; i++) {
			jaf.ui.showNotification(messages[i]);
		}
	});

}(window.jaf = window.jaf || {}, jQuery));