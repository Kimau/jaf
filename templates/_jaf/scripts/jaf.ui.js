(function (ui, $, undefined) {

	ui.showNotification = function (message) {
		let notif_container = $("<div></div>");
		notif_container.addClass('notification');
		notif_container.addClass('notification-' + message.type);
		notif_container.html(message.message);

		$('#notification-area').append(notif_container);
		notif_container.slideDown(200);

		setTimeout(function () {
				notif_container.slideUp(300, function () {
					$(this).remove();
				});
			},
			5000);
	};

	ui.ajaxPrompt = function (title, description, action, requestData) {
		createPopup();

		let textContent = "<h2>" + title + "</h2>" + description + "<br /><br />";
		let buttons = "<a id=\"button-cancel\" class=\"button\">Cancel</a>";
		buttons += "<a id=\"button-confirm\" class=\"button\">Confirm</a>";

		fillPopup("<div class=\"center\">" + textContent + buttons + "</div>");

		$('#button-cancel').on('click', function () {
			closePopup();
		});

		$('#button-confirm').on('click', function () {
			hidePopup();

			// TODO: Add loading spinner
			$.post(action, {
				data: requestData
			}).done(function (returnData) {
				returnData = JSON.parse(returnData);
				ui.messagePopup(returnData.message, '')
			}).fail(function () {
				jaf.ui.messagePopup('AJAX Failed', 'Failed loading necessary resources')
			});
		});
	};

	ui.messagePopup = function (title, description) {
		createPopup();

		let textContent = "<h2>" + title + "</h2>" + description + "<br /><br />";
		let button = "<a id=\"button-close\" class=\"button\">Close</a>";

		fillPopup("<div class=\"center\">" + textContent + button + "</div>");

		$('#button-close').on('click', function () {
			closePopup();
		});
	};

	ui.customPopup = function (content) {
		createPopup();
		fillPopup(content);
	};

	function createPopup() {
		if (!$('#popupOverlay').length) {
			$('body').prepend('<div id=\'popupOverlay\'></div>');
			$('#popupOverlay').fadeIn(300);
		}

		if (!$('#uiPopup').length) {
			$('body').append('<div id=\'uiPopup\'></div>');
		}
	}

	function fillPopup(content) {
		$('#uiPopup').html(content);
		showPopup();
	}

	function hidePopup() {
		$('#uiPopup').fadeOut(200, function () {
			$(this).hide()
		});
	}

	function showPopup() {
		$('#uiPopup').fadeIn(200);
	}

	function closePopup() {
		$('#uiPopup').fadeOut(200, function () {
			$(this).remove()
		});
		$('#popupOverlay').fadeOut(300, function () {
			$(this).remove()
		});
	}

}(window.jaf.ui = window.jaf.ui || {}, jQuery));