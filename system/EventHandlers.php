<?php

namespace JAF;

class EventHandlers
{
	private $eventHandlers = [];

	/**
	 * EventHandlers constructor. Fetches all event handlers from DB
	 */
	public function __construct()
	{
		$query = db()->prepare('SELECT * FROM ' . TABLE_EVENT_HANDLERS . ' AS h
                                LEFT JOIN ' . TABLE_MODULES . ' AS m
                                ON m.name = h.module
                                WHERE m.enabled = 1');
		$query->execute();
		foreach ($query->fetchAll() as $eventHandler)
		{
			$this->addHandler($eventHandler['event'], $eventHandler['module'], $eventHandler['handler']);
		}
	}

	/**
	 * Adds an event handler which will call a function in a module upon an event triggering
	 *
	 * @param string $event Unique name of the event
	 * @param string $module Which module who has a handler for the event
	 * @param string $handler Which function to run in the module
	 */
	public function addHandler($event, $module, $handler)
	{
		if (!isset($this->eventHandlers[$event]))
		{
			$this->eventHandlers[$event] = [];
		}

		$this->eventHandlers[$event][] = [
			'module' => $module,
			'handler' => $handler
		];
	}

	/**
	 * Triggers an event which calls all functions which has a handler for it
	 *
	 * @param string $event Unique name of the event
	 * @param array $args Arguments to pass onto event handler
	 * @return array
	 */
	public function triggerEvent($event, $args = [])
	{
		if (!isset($this->eventHandlers[$event]))
		{
			return [];
		}

		$result = [];
		foreach ($this->eventHandlers[$event] as $handler)
		{
			$data = call_user_func_array([jaf()->modules->getModule($handler['module']), $handler['handler']], $args);
			if (!empty($data)) // Empty results should be ignored
			{
				$result[] = $data;
			}
		}

		return $result;
	}
}