<?php

namespace JAF;

class Modules
{
	/** @var \JAF\Modules\Module[] Cache of loaded modules */
	private $modulesCache = [];

	/** @var string[] List of enabled modules */
	private $enabledModules = [];

	/** @var string Name of currently active module, this might influence for instance what JS is run */
	private $active_module;

	/**
	 * Modules constructor.
	 *
	 * Gets list of enabled modules from DB
	 */
	public function __construct()
	{
		$query = db()->prepare("SELECT name FROM " . TABLE_MODULES . " WHERE enabled = 1");
		$query->execute();
		$this->enabledModules = array_column($query->fetchAll(), 'name');
	}

	/**
	 * @param string $module_name
	 * @return \JAF\Modules\Module Returns a specific module
	 */
	public function getModule($module_name)
	{
		if (!isset($this->modulesCache[$module_name]) || !is_object($this->modulesCache[$module_name]))
		{
			if ($this->moduleExists($module_name))
			{
				$class = '\\JAF\\Modules\\' . $module_name . '\\' . $module_name;
				$modules[$module_name] = new $class();
			}
			else
			{
				Error::fatal(lang('LOAD_UNDEFINED_MODULE', null, [$module_name]));
			}
		}

		return $modules[$module_name];
	}

	/**
	 * @return string
	 */
	public function getActiveModuleName()
	{
		return $this->active_module;
	}

	/**
	 * @param string $module
	 */
	public function setActiveModule($module)
	{
		$this->active_module = $module;
	}

	/**
	 * @param string $module_name
	 * @return bool
	 */
	public function moduleExists($module_name)
	{
		$class = '\\JAF\\Modules\\' . $module_name . '\\' . $module_name;

		return (in_array($module_name, $this->enabledModules) && class_exists($class));
	}
}