<?php

namespace JAF;

class TemplateHandler
{
	/** @var string|null */
	public $module;

	/** @var string */
	public $template_file;

	/** @var string[] */
	public $template_vars = [];

	/** @var self[] Other TemplateHandler objects which are rendered as part of this one */
	public $sub_views = [];

	/**
	 * TemplateHandler constructor.
	 *
	 * @param string|$template_file
	 * @param string|null $module
	 */
	public function __construct($template_file, $module = null)
	{
		$this->module = $module;
		$this->template_file = $template_file;
	}

	/**
	 * @param string $key
	 * @param string|array $val
	 */
	public function setVariable($key, $val)
	{
		$this->template_vars[$key] = $val;
	}

	/**
	 * @param string $key
	 * @param self $template_handler
	 */
	public function addSubView($key, $template_handler)
	{
		if (!isset($this->sub_views[$key]))
		{
			$this->sub_views[$key] = [];
		}

		$this->sub_views[$key][] = $template_handler;
	}

	/**
	 * @param array $var_array
	 */
	public function setVariables($var_array)
	{
		$this->template_vars = array_merge($this->template_vars, $var_array);
	}
}