<?php

namespace JAF;

class Jaf
{
	/** @var string Path after index.php */
	private $path;

	/** @var array Messages that should be displayed to user */
	private $messages = [];

	// Core class-instances
	/** @var \JAF\Config */
	public $config;
	/** @var \JAF\Database */
	public $database;
	/** @var \JAF\EventHandlers */
	public $eventHandlers;
	/** @var \JAF\Groups */
	public $groups;
	/** @var \JAF\Language */
	public $language;
	/** @var \JAF\Modules */
	public $modules;
	/** @var \JAF\Paths */
	public $paths;
	/** @var \JAF\Renderer */
	public $renderer;
	/** @var \JAF\Session */
	public $session;
	/** @var \JAF\User */
	public $user;

	/**
	 * Jaf constructor.
	 */
	public function __construct()
	{
		// We only care for path after index.php.
		$this->path = (isset($_SERVER['PATH_INFO'])) ? $_SERVER['PATH_INFO'] : '/';
	}

	/**
	 * Initiates all system classes, and serves the page.
	 */
	public function execute()
	{
		// Loading error-display necessities first
		$this->config = new Config();
		$this->database = new Database();

		require_once(config('paths', 'base_path') . '/constants.php');

		$this->language = new Language();
		$this->renderer = new Renderer();
		$this->paths = new Paths();
		$this->eventHandlers = new EventHandlers();
		$this->modules = new Modules();
		$this->groups = new Groups();
		$this->session = new Session();
		$this->user = new User(); // Needs to be loaded after session and groups

		// Check if the user attempts to login
		if (!$this->user->verifyLoggedIn())
		{
			if (isset($_POST['username']) && isset($_POST['password']))
			{
				$long_session = (isset($_POST['long_session'])) ? true : false; // Checkboxes are either set or not
				if ($this->user->processLogin($_POST['username'], $_POST['password'], $long_session))
				{
					$this->addMessage('success', "Login sucessful");
				}
				else
				{
					$this->addMessage('failure', "Login failed");
				}
			}
		}

		// TODO: Only temporary, should be done on-demand rather than autonomous
		$this->renderer->compileScss();
		$this->renderer->implodeJs();

		$page = $this->paths->serve($this->path);

		echo $this->renderer->renderPage($page);
	}

	/**
	 * @return string Path relative to the file that executes Jaf. (/web/index.php)
	 */
	public function getCurrentPath()
	{
		return $this->path;
	}

	/**
	 * Adds a message to be shown to the user
	 *
	 * @param string $type Type of message: 'failure', 'notice', or 'success'. Other types may not display correctly
	 * @param string $message
	 */
	public function addMessage($type, $message)
	{
		$this->messages[] = [
			'type' => $type,
			'message' => $message
		];
	}

	/**
	 * @return array All messages which are to be displayed to user
	 */
	public function getMessages()
	{
		return $this->messages;
	}
}