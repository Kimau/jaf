<?php

namespace JAF;

class User
{
	/** @var int */
	private $user_id;

	/**
	 * User constructor. Loads user data based on session
	 */
	public function __construct()
	{
		$this->user_id = jaf()->session->getUserId();

		$query = db()->prepare('SELECT * FROM ' . TABLE_USERS . ' WHERE user_id = :user_id');
		$query->execute([
			'user_id' => $this->user_id
		]);

		if ($query->rowCount() == 0)
		{
			// This is probably very bad, do the courtesy of regenerating session and show fatal error
			// TODO: Block IP for a while if this happens several times
			jaf()->session->generateSession(ANONYMOUS_USER_ID);
			Error::fatal(lang('INVALID_USER_ID'));
		}

		$user_data = $query->fetch();

		//$this->setPassword('kimau', 'testing');
	}

	/**
	 * @return int
	 */
	public function getIdFromCurrentUser()
	{
		return $this->user_id;
	}

	/**
	 * @param $username
	 * @param $password
	 */
	public function setPassword($username, $password)
	{
		$query = db()->prepare('UPDATE ' . TABLE_USERS . ' SET password = :password WHERE username = :username');
		$query->execute([
			'username' => $username,
			'password' => password_hash($password, PASSWORD_DEFAULT, ['cost' => 12])
		]);
	}

	/**
	 * @return bool True if user is logged in
	 */
	public function verifyLoggedIn()
	{
		return ($this->user_id != ANONYMOUS_USER_ID);
	}

	/**
	 * @param string $username
	 * @param string $password
	 * @param bool $stay_logged_in Tells \JAF\Session to use long session
	 * @return bool True if login attempt is successful
	 */
	public function processLogin($username, $password, $stay_logged_in)
	{
		// TODO: Take action upon many failed attempts from same IP?

		$query = db()->prepare('SELECT * FROM ' . TABLE_USERS . ' WHERE username = :username');
		$query->execute([
			'username' => $username
		]);

		if ($query->rowCount() == 0)
		{
			Error::log(lang('INVALID_LOGIN_USERNAME', null, [$username]));
			return false;
		}

		$user_data = $query->fetch();

		if (!password_verify($password, $user_data['password']))
		{
			Error::log(lang('INVALID_LOGIN_PASSWORD', null, [$username]));
			return false;
		}

		$this->user_id = $user_data['user_id'];
		jaf()->session->generateSession($user_data['user_id'], $stay_logged_in);

		return true;
	}
}