<?php

namespace JAF;

class Language
{
	/**
	 * @var string[][] All language strings loaded, ordered by module
	 */
	private $languageCache = [];

	/**
	 * Language constructor.
	 */
	public function __construct()
	{
		// Load core language immediately
		$this->loadLanguageFile(null);
	}

	/**
	 * @param string $key
	 * @param string|null $module Null is handled as core module
	 * @param string[]|null $args Uses vnsprintf() (in common.php) to substitute in associate arrays. Syntax: %key$s
	 * @return string
	 */
	public function getString($key, $module = null, $args = null)
	{
		if (!isset($this->languageCache[$module]))
		{
			$this->loadLanguageFile($module); // Null keys will be interpreted as "" by PHP
		}

		if (!isset($this->languageCache[$module][$key]))
		{
			// Prevent an infinite loop here
			if ($key === 'MISSING_LANGUAGE_KEY')
			{
				Error::fatal("Can't find language key: MISSING_LANGUAGE_KEY (infinite loop)");
			}

			Error::warning($this->getString('MISSING_LANGUAGE_KEY', null, [$key]));
			return $key;
		}

		if (is_array($args))
		{
			return vnsprintf($this->languageCache[$module][$key], $args);
		}
		else
		{
			return $this->languageCache[$module][$key];
		}
	}

	/**
	 * @param string|null $module Null is handled as core language
	 */
	private function loadLanguageFile($module)
	{
		$path = $this->buildLanguagePath($module);

		$file_content = file_get_contents($path);
		$this->languageCache[$module] = json_decode($file_content, true);
	}

	/**
	 * @param string|null $module Null is handled as core language files
	 * @param string|null $language Null is handled as default language
	 * @return string
	 */
	private function buildLanguagePath($module = null, $language = null)
	{
		$path = config('paths', 'base_path');

		if (!is_null($module))
		{
			if (!jaf()->modules->moduleExists($module))
			{
				Error::fatal("Tried loading language file from undefined module: " . $module);
			}

			$path .= config('paths', 'modules_dir') . "/" . $module . "/languages";
		}
		else
		{
			$path .= config('paths', 'languages_dir');
		}

		if (file_exists($path . "/" . $language . ".json"))
		{
			$path .= "/" . $language . ".json";
		}
		else
		{
			$path .= "/" . config('general', 'default_language') . ".json";
		}

		if (!file_exists($path))
		{
			Error::fatal("Language file not found: " . $path);
		}

		return $path;
	}
}