<?php

namespace JAF;

// TODO: Revise how permissions are handled

class Groups
{
	/**
	 * @param int $group_id
	 * @param string $permission_key
	 * @return bool True if a group has a certain permission
	 */
	public function verifyGroupPermission($group_id, $permission_key)
	{
		$query = db()->prepare('SELECT * FROM  ' . TABLE_GROUP_PERMISSIONS . '
								WHERE group_id = :group_id AND permission_key = :permission__key');
		$query->execute([
			'group_id' => $group_id,
			'permission_key' => $permission_key
		]);

		if ($query->rowCount() == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * @param int $user_id
	 * @param string $permission_key
	 * @return bool True if a group has a certain permission
	 */
	public function verifyUserPermission($user_id, $permission_key)
	{
		$query = db()->prepare('SELECT * FROM  ' . TABLE_GROUP_PERMISSIONS . ' AS p
										INNER JOIN ' . TABLE_GROUP_MEMBERS . ' AS g
										ON p.group_id = g.group_id
										WHERE g.user_id = :user_id AND p.permission_key = :permission_key');
		$query->execute([
			'user_id' => $user_id,
			'permission_key' => $permission_key
		]);

		if ($query->rowCount() == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * @param int $user_id
	 * @return array Groups a specific user is part of
	 */
	public function getGroupsByUserId($user_id)
	{
		$query = db()->prepare('SELECT g.* FROM ' . TABLE_GROUPS . ' g
								LEFT JOIN ' . TABLE_GROUP_MEMBERS . ' as gm
								ON g.id = gm.group_id
								WHERE gm.user_id = :user_id');
		$query->execute([
			'user_id' => $user_id
		]);

		return $query->fetchAll();
	}

	public function getGroupByGroupId($group_id)
	{
		return [];
	}

	public function getGroupsByPermission($permission_key)
	{
		return [];
	}
}