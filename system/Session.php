<?php

namespace JAF;

// TODO: Revise datetime handling a bit.

class Session
{
	/** @var string Unique ID denominating the session */
	private $session_id;

	/** @var string */
	private $ip_address;

	/** @var string */
	private $user_agent;

	/** @var int */
	private $user_id;

	/** @var \DateTime When the session expires */
	private $expire_datetime;

	/** @var bool If set to true the long_session config item is used as expiry time */
	private $long_session = false;

	/**
	 * Session constructor. Makes sure that all visitors have a session, even anonymous users.
	 */
	public function __construct()
	{
		$this->ip_address = $_SERVER['REMOTE_ADDR']; // TODO: Store all IP addresses used on current session
		$this->user_agent = $_SERVER['HTTP_USER_AGENT'];

		if (!$this->readSessionCookie())
		{
			$this->generateSession(ANONYMOUS_USER_ID);
			return;
		}

		$query = db()->prepare('SELECT * FROM  ' . TABLE_SESSIONS . ' WHERE session_id = :session_id');
		$query->execute([
			'session_id' => $this->session_id
		]);

		if ($query->rowCount() == 0)
		{
			Error::log(lang('SESSION_INVALID_ID', null));
			$this->generateSession(ANONYMOUS_USER_ID);
			return;
		}

		$session_data = $query->fetch();
		$this->user_id = $session_data['user_id'];
		$this->long_session = boolval($session_data['long_session']);

		try
		{
			$db_expire = new \DateTime($session_data['expires']);
		} catch (\Exception $e)
		{
			Error::fatal($e->getMessage());
		}

		// TODO: Quick reload forces this
		// Check if data in cookie match data in DB
		if ($db_expire != $this->expire_datetime)
		{
			Error::log(lang('SESSION_INVALID_EXPIRE', null, [$this->session_id, $db_expire->format('Y-m-d H:i:s'), $this->expire_datetime->format('Y-m-d H:i:s')]));
			$this->generateSession(ANONYMOUS_USER_ID);
			return;
		}

		// Has session expired?
		if ($this->expire_datetime < getDateTimeObj('now', 'UTC'))
		{
			$this->generateSession(ANONYMOUS_USER_ID);
			return;
		}

		// Has the user changed browser?
		if ($this->user_agent !== $session_data['user_agent']) // TODO: Not log out user when they update their browser
		{
			$this->generateSession(ANONYMOUS_USER_ID);
			Error::log(lang('SESSION_USER_AGENT_CHANGED', null, [$this->user_id, $this->session_id]));
			return;
		}

		// Session is valid, refresh it
		$this->refreshSession();
	}

	/**
	 * Generates a session for the user. Assumes validated user id is provided
	 *
	 * @param int $user_id User id
	 * @param bool $long_session Determines if the sessions should not expire
	 */
	public function generateSession($user_id = ANONYMOUS_USER_ID, $long_session = false)
	{
		// Terminate current session if it exist
		$this->destroyCurrentSession();

		$this->user_id = $user_id;
		$this->session_id = openssl_random_pseudo_bytes(32);
		$this->long_session = $long_session;

		try
		{
			if ($this->long_session)
			{
				$expire_margin = new \DateInterval(config('session', 'long_expire_time'));
			}
			else
			{
				$expire_margin = new \DateInterval(config('session', 'expire_time'));
			}
		} catch (\Exception $e)
		{
			Error::fatal($e->getMessage());
		}
		$this->expire_datetime = getDateTimeObj('now', 'UTC');
		$this->expire_datetime->add($expire_margin);

		$current_datetime_str = getMysqlDateTimeStr();
		$expires_datetime_str = $this->expire_datetime->format('Y-m-d H:i:s');
		$query = db()->prepare('INSERT INTO  ' . TABLE_SESSIONS . ' VALUES (:session_id, :ip_address, :user_agent, :created, :expires, :last_activity, :user_id, :long_session)');
		$query->execute([
			'session_id' => $this->session_id,
			'ip_address' => $this->ip_address,
			'user_agent' => $this->user_agent,
			'created' => $current_datetime_str,
			'expires' => $expires_datetime_str,
			'last_activity' => $current_datetime_str,
			'user_id' => $this->user_id,
			'long_session' => intval($this->long_session)
		]);

		$this->setSessionCookie();
	}

	/**
	 *  Sets expire-time to now in DB and in cookie
	 */
	public function destroyCurrentSession()
	{
		$this->expire_datetime = getDateTimeObj('now', 'UTC');
		$query = db()->prepare('UPDATE ' . TABLE_SESSIONS . ' SET expires = :expires WHERE session_id = :session_id AND expires > UTC_TIMESTAMP()');
		$query->execute([
			'session_id' => $this->session_id,
			'expires' => $this->expire_datetime->format('Y-m-d H:i:s')
		]);

		$this->setSessionCookie();
	}

	/**
	 * @return int
	 */
	public function getUserId()
	{
		return $this->user_id;
	}

	/**
	 * Updates the expiry date of cookie and in DB, also updates last_activity in DB
	 */
	private function refreshSession()
	{
		try
		{
			if ($this->long_session)
			{
				$expire_margin = new \DateInterval(config('session', 'long_expire_time'));
			}
			else
			{
				$expire_margin = new \DateInterval(config('session', 'expire_time'));
			}
		} catch (\Exception $e)
		{
			Error::fatal($e->getMessage());
		}
		$this->expire_datetime = getDateTimeObj('now', 'UTC');
		$this->expire_datetime->add($expire_margin);

		$current_datetime_str = getMysqlDateTimeStr();
		$query = db()->prepare('UPDATE  ' . TABLE_SESSIONS . ' SET expires = :expires, last_activity = :last_activity WHERE session_id = :session_id');
		$query->execute([
			'session_id' => $this->session_id,
			'expires' => $this->expire_datetime->format('Y-m-d H:i:s'),
			'last_activity' => $current_datetime_str
		]);

		$this->setSessionCookie();
	}

	/**
	 * Reads the session-ID from the cookie and updates the private variable session_id.
	 *
	 * @return bool True if cookies exist.
	 */
	private function readSessionCookie()
	{
		if (!isset($_COOKIE[config('session', 'cookie_name')]))
		{
			return false;
		}

		$raw_cookie_data = explode('|', base64_decode($_COOKIE[config('session', 'cookie_name')]));

		// Check if the hash matches
		if (!password_verify($raw_cookie_data[0] . config('session', 'hash_salt'), $raw_cookie_data[1]))
		{
			Error::log(lang('INVALID_SESSION_HASH', null, [$this->session_id]));
			return false;
		}

		$cookie_data = explode('.', $raw_cookie_data[0]);
		$expire = getDateTimeObj($cookie_data[1], 'UTC');
		// Save ourselves some effort and abort here if cookie is invalid
		if ($expire < getDateTimeObj('now', 'UTC'))
		{
			return false;
		}

		$this->session_id = base64_decode($cookie_data[0]);
		$this->expire_datetime = $expire;

		return true;
	}

	/**
	 *  Sets the session cookie with parameters from config
	 */
	private function setSessionCookie()
	{
		$raw_data = base64_encode($this->session_id) . "." . $this->expire_datetime->format('Y-m-d H:i:s');
		$hashed_data = password_hash($raw_data . config('session', 'hash_salt'), PASSWORD_DEFAULT);

		setcookie(
			config('session', 'cookie_name'),
			base64_encode($raw_data . "|" . $hashed_data),
			$this->expire_datetime->getTimestamp(),
			config('session', 'cookie_path'),
			config('session', 'cookie_domain'),
			config('session', 'cookie_secure'),
			config('session', 'cookie_httponly')
		);
	}
}