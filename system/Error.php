<?php

namespace JAF;

class Error
{
	/**
	 * @var bool Set to true upon fatal errors, will prevent infinite error loops
	 */
	private static $fatal_occured = false;

	/**
	 *  Redirects (fatal) PHP-internal errors to self::ErrorHandler()
	 *
	 * @return bool If false PHP-internal error handling will take over
	 */
	public static function phpErrorHandler()
	{
		$error = error_get_last();

		if (!is_null($error))
		{
			return self::errorHandler($error['type'], $error['message'], $error['file'], $error['line']);
		}
		return false;
	}

	/**
	 * Handles error in a way we want to: custom template, custom logging, custom output
	 *
	 * @param int $error_type PHP internal error type
	 * @param string $error_str
	 * @param string $error_file
	 * @param int $error_line
	 * @return bool PHP internal error handling upon false. Cascades to Self::fatalErrorHandler()
	 */
	public static function errorHandler($error_type, $error_str, $error_file, $error_line)
	{
		// This error code is not included in error_reporting, so let it fall
		// through to the standard PHP error handler
		if (!(error_reporting() & $error_type))
		{
			return false;
		}

		$fatal = false;

		switch ($error_type)
		{
			case E_STRICT:
			case E_NOTICE:
			case E_USER_NOTICE:
				$message = "(" . $error_type . ") " . $error_str . " on line " . $error_line . " in file " . $error_file . "\n";
				self::writeToLog('NOTICE', $message);
				break;
			case E_WARNING:
			case E_USER_WARNING:
				$message = "(" . $error_type . ") " . $error_str . " on line " . $error_line . " in file " . $error_file . "\n";
				self::writeToLog('WARNING', $message);
				break;
			case E_RECOVERABLE_ERROR:
			case E_ERROR:
			case E_USER_ERROR:
			case E_PARSE:
			default:
				$fatal = true;
				$message = "(" . $error_type . ") " . $error_str . "\n";
				$message .= "Fatal error on line " . $error_line . " in file " . $error_file;
				self::writeToLog('FATAL', $message);
				break;
		}

		if ($fatal === true)
		{
			// If a fatal has already occured we need to stop now (because the error is probably in renderer or language)
			if (self::$fatal_occured)
			{
				exit("A fatal error has occured. Please contact a system administrator, or your supervisor.");
			}

			self::$fatal_occured = true;

			// Can't output nice error page without core initialized
			if (!self::jafInitialized())
			{
				exit(lang('FATAL_ERROR_DESC'));
			}

			jaf()->renderer->renderError(lang('FATAL_ERROR_DESC'));

			exit(); // It is redundant here (run in renderer), but better safe than sorry
		}

		// Don't execute PHP internal error handler
		return true;
	}

	/**
	 * Triggers a log write
	 *
	 * @param string $description
	 */
	public static function log($description)
	{
		self::writeToLog('LOG', $description);
	}

	/**
	 * Triggers a warning
	 *
	 * @param string $description
	 */
	public static function warning($description)
	{
		$debug = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
		self::errorHandler(
			E_USER_WARNING,
			$description,
			$debug[0]['file'],
			$debug[0]['line']);
	}

	/**
	 * Triggers a fatal error
	 *
	 * @param string $description
	 */
	public static function fatal($description)
	{
		$debug = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
		self::errorHandler(
			E_USER_ERROR,
			$description,
			$debug[0]['file'],
			$debug[0]['line']);

		exit(); // It is redundant here (run in renderer and errorHandler), but better safe than sorry
	}

	/**
	 * Triggers a 404 error
	 */
	public static function httpError()
	{
		self::log('Path error: ' . jaf()->getCurrentPath());
		jaf()->renderer->renderError('404'); // TODO: Better description :P
	}

	/**
	 * Triggers a 403 error
	 */
	public static function authError()
	{
		self::log('Auth error: ' . jaf()->getCurrentPath());
		jaf()->renderer->renderError('403'); // TODO: Better description :P
	}

	/**
	 * Writes information into the logs
	 *
	 * @param string $type
	 * @param string $message
	 */
	private static function writeToLog($type, $message)
	{
		if (!config('general', 'log_enabled'))
		{
			return;
		}

		$time_string = getTimeStr();
		$date_string = getCustomDateTimeStr('now', config('datetime', 'server_timezone'), 'Y-m-d');

		$file_path = config('paths', 'base_path') . config('paths', 'log_dir') . '/' . $date_string;

		if (!$file_handler = fopen($file_path, 'a'))
		{
			echo("Unable to open log file");
			return;
		}

		$log_str = $time_string . " [" . strtoupper($type) . "] " . $message . "\n";
		fwrite($file_handler, $log_str);

		fclose($file_handler);
	}

	/**
	 * @return bool True if \JAF\JAF is loaded
	 */
	private static function jafInitialized()
	{
		return (function_exists('jaf') && is_object(jaf()) && is_object(jaf()->renderer));
	}
}