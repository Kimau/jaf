<?php

namespace JAF;

class Renderer
{
	/** @var \Smarty */
	private $smarty;

	/** @var string Which template will be used under rendering */
	private $template;

	/** $var string Which output mode to use in renderPage(). 'web', 'ajax'  is accepted */
	private $output_mode = 'web';

	/**
	 * Renderer constructor.
	 */
	public function __construct()
	{
		try
		{
			$this->smarty = new \Smarty();

			$this->smarty->setCompileDir(config('paths', 'base_path') . config('smarty', 'compile_dir'));
			$this->smarty->registerPlugin('block', 'ln', [$this, 'smartyLanguagePlugin']);
			$this->smarty->force_compile = config('smarty', 'force_compile');
		}
		catch (\Exception  $e)
		{
			Error::fatal($e->getMessage());
		}

		$this->template = config('general', 'default_template');
	}

	/**
	 * Renders a template file and returns content
	 *
	 * @param \JAF\TemplateHandler $template_handler
	 * @return string
	 */
	public function renderFile($template_handler)
	{
		$this->smarty->clearAllAssign();

		$template_file_path = $this->buildTplPath($template_handler->template_file, $template_handler->module);

		$rendered_sub_views = [];
		foreach ($template_handler->sub_views as $key => $sub_views)
		{
			$rendered_sub_views[$key] = [];
			foreach ($sub_views as $sub_view)
			{
				$rendered_sub_views[$key][] = $this->renderFile($sub_view);
			}
		}
		$this->smarty->assign($rendered_sub_views);
		$this->smarty->assign($template_handler->template_vars);

		try
		{
			return $this->smarty->fetch($template_file_path);
		} catch (\Exception $e)
		{
			Error::fatal($e->getMessage());
		}
	}

	/**
	 * Renders a page with header and footer
	 *
	 * @param \JAF\TemplateHandler|string $outputContent
	 * @return string
	 */
	public function renderPage($outputContent)
	{
		// Parse given content correctly dependent on type
		if ($outputContent instanceof TemplateHandler)
		{
			$outputContent = $this->renderFile($outputContent);
		}
		else
		{
			$outputContent = print_r($outputContent, true);
		}

		switch($this->output_mode)
		{
			case 'ajax':
				$output = $outputContent;
				break;

			case 'web':
			default:
				$output = $this->renderHeader();
				$output .= $outputContent;
				$output .= $this->renderFooter();
				break;
		}

		return $output;
	}

	/**
	 *   Renders a login-box and exits
	 */
	public function renderLogin()
	{
		$login_tpl = new TemplateHandler('login');
		$output = $this->renderPage($login_tpl);

		exit($output);
	}

	/**
	 * Displays a nice error message with normal page layout and exits
	 * @param $message string Message to be rendered
	 */
	public function renderError($message)
	{
		$error_tpl = new TemplateHandler('error');
		$error_tpl->setVariable('message', $message);
		$output = $this->renderPage($error_tpl);

		exit($output);
	}


	/**
	 * Renders the page header
	 *
	 * @return string Header HTML
	 */
	private function renderHeader()
	{
		$template_object = new TemplateHandler('header');

		$template_object->setVariables([
			'title' 		=> config('general', 'page_title'),
			'template' 		=> $this->template,
			'active_module' => jaf()->modules->getActiveModuleName(),
			'messages' 		=> json_encode(jaf()->getMessages())
		]);

		return $this->renderFile($template_object);
	}

	/**
	 * Renders the page footer
	 *
	 * @return string Header HTML
	 */
	private function renderFooter()
	{
		$template_object = new TemplateHandler('footer');

		return $this->renderFile($template_object);
	}

	/**
	 * @param string $file Name template file to build path to, without extension
	 * @param string|null $module Module to build a template file path to, null is core
	 * @return string Path to template file
	 */
	private function buildTplPath($file, $module = null)
	{
		$path = config('paths', 'base_path');

		if (!is_null($module))
		{
			$path .= config('paths', 'modules_dir') . '/' . $module . '/templates';
		}
		else
		{
			$path .= config('paths', 'templates_dir');
		}

		if (is_dir($path . '/' . $this->template))
		{
			$path .= '/' . $this->template;
		}
		else
		{
			$path .= '/' . config('general', 'default_template');
		}
		$path .= '/' . $file . '.tpl';

		if (!file_exists($path))
		{
			Error::fatal("Template not found: " . $path);
		}

		return $path;
	}

	/**
	 * Sets which template will be loaded
	 *
	 * @param string $template
	 */
	public function setTemplate($template)
	{
		$this->template = $template;
	}

	/**
	 * Sets which output-mode to use in renderPage()
	 *
	 * 'web': Outputs full page with footer and header.
	 * 'ajax': Outputs only a single template file or alternatively a string
	 *
	 * @param string $mode
	 */
	public function setOutputMode($mode)
	{
		$this->output_mode = $mode;
	}

	/**
	 * Loads all JS from the scripts directories for each template in the environment and combines them to one file
	 * for each template.
	 */
	public function implodeJs()
	{
		// TODO: Can the implementation for finding files be optimized?
		$raw_code = [];

		// glob() uses some form of regex, see documentation for details.
		$core_template_path = config('paths', 'base_path') . config('paths', 'templates_dir') . '/*/scripts/*.js';
		$modules_template_path = config('paths', 'base_path') . config('paths', 'modules_dir') . '/*/templates/*/scripts/*.js';

		foreach (array_merge(glob($core_template_path, GLOB_BRACE), glob($modules_template_path, GLOB_BRACE)) as $file)
		{
			$path_matches = [];
			$path_regexp = "#(?:modules/([_a-zA-Z]+?)/)?templates/([_a-zA-Z]+?)/scripts#";
			preg_match($path_regexp, $file, $path_matches);

			// Skip disabled modules
			if(!empty($path_matches[1]) && !jaf()->modules->moduleExists($path_matches[1]))
			{
				continue;
			}

			$code = file_get_contents($file);

			// Language conversion
			$language_regexp = "#(?<d>['\"])(?<pre_string>.*?)\{ln(?<args> [a-z]+?=.+?)?\}(?<key>.*?)\{/ln\}#";
			$code = preg_replace_callback($language_regexp, [$this, 'jsLanguagePlugin'], $code);

			$raw_code[$path_matches[2]][] = $code;
		}

		foreach ($raw_code as $template => $code_array)
		{
			$imploded_code = implode("\n", $code_array);

			$file_path = config('paths', 'base_path') . config('paths', 'web_dir') . "/scripts/" . $template . ".js";
			$file_handle = fopen($file_path, 'w+');
			fwrite($file_handle, $imploded_code);
			fclose($file_handle);
		}
	}

	/**
	 * Loads all scss and cs from the scripts directories for each template in the environment and combines them to one file
	 * for each template. The scss is also compiled to css.
	 */
	public function compileScss()
	{
		$uncompiled_code = [];

		// glob() uses some form of regex, see documentation for details.
		$core_path = config('paths', 'base_path') . config('paths', 'templates_dir') . '/*/styles/*.{scss,css}';
		$modules_path = config('paths', 'base_path') . config('paths', 'modules_dir') . '/*/templates/*/styles/*.{scss,css}';

		foreach (array_merge(glob($core_path, GLOB_BRACE), glob($modules_path, GLOB_BRACE)) as $file)
		{
			$path_matches = [];
			preg_match('#(?:modules/([a-zA-Z]+?)/)?templates/([_a-zA-Z]+?)/styles#', $file, $path_matches);

			if(empty($path_matches[1]) || jaf()->modules->moduleExists($path_matches[1]))
			{
				$uncompiled_code[$path_matches[2]][] = file_get_contents($file);
			}
		}

		$scss = new \scssc();
		$scss->setFormatter('scss_formatter_compressed');
		foreach ($uncompiled_code as $template => $code_array)
		{
			$imploded_code = implode("\n", $code_array);
			$compiled_code = $scss->compile($imploded_code);

			$file_path = config('paths', 'base_path') . config('paths', 'web_dir') . "/styles/" . $template . ".css";
			$file_handle = fopen($file_path, 'w+');
			fwrite($file_handle, $compiled_code);
			fclose($file_handle);
		}
	}

	/**
	 * Used as a callback in preg_replace_callback() for language support in JS
	 *
	 * @param string[] $matches
	 * @return string
	 */
	public function jsLanguagePlugin($matches)
	{
		$args = [];
		$module = null;

		if(isset($matches['args']))
		{
			$arg_array = explode(" ", $matches['args']);
			array_shift($arg_array); // Remove first element (empty)

			foreach($arg_array as $arg)
			{
				$arg_components = explode('=', $arg);

				// m is treated specially to define which module to load from
				if($arg_components[0] == 'm')
				{
					$module = $arg_components[1];
					continue;
				}

				// Add correct type of quotes, and concatenate in argument as a JS variable
				$args[$arg_components[0]] = $matches['d'] . "+" . $arg_components[1] . "+" . $matches['d'];
			}
		}

		// Re add the quote and parts of the string we had to remove during the search process.
		return $matches['d'] . $matches['pre_string'] . lang($matches['key'], $module, $args);
	}

	/**
	 * Wrapper to give access to language strings from template files
	 *
	 * @param array $params
	 * @param string $content
	 * @param \Smarty_Internal_Template $template
	 * @param bool $repeat True upon opening tag, false upon closing tag
	 * @return string Interpreted language string
	 */
	public function smartyLanguagePlugin($params, $content, $template, &$repeat)
	{
		if (!$repeat && isset($content))
		{
			if(isset($params['m']))
			{
				// Unset module so it isn't sent as an argument
				$module = $params['m'];
				unset($params['m']);
			}
			else
			{
				$module = null;
			}

			return lang($content, $module, $params);
		}
	}
}