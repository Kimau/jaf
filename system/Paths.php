<?php

namespace JAF;

class Paths
{
	private $paths = [];

	/**
	 * Paths constructor.
	 * Loads available paths from DB
	 */
	public function __construct()
	{
		$query = db()->prepare('SELECT p.module, p.path, p.handler FROM ' . TABLE_PATHS . ' AS p
                               LEFT JOIN ' . TABLE_MODULES . ' AS m
                               ON m.name = p.module
                               WHERE m.enabled = 1');
		$query->execute();
		$this->paths = $query->fetchAll();
	}

	/**
	 * @param string $module Module to load upon path serving
	 * @param string $path Path to serve
	 * @param string $handler Which function in the module to run.
	 */
	public function definePath($module, $path, $handler)
	{
		$this->paths[] = [
			'module' => $module,
			'path' => $path,
			'handler' => $handler
		];
	}

	/**
	 * Serves a path by loading a specific module
	 *
	 * @param string $uri Path to serve
	 * @return string
	 */
	public function serve($uri)
	{
		foreach ($this->paths as $path)
		{
			// TODO: Create regexp keywords that can be used in paths to collect information. NB: Strict validation!
			$regexp_shortcuts = [
				'[*]' => "(.*?)",
				'[INT]' => "([1-9]+)"
			];

			$args = [];
			if (preg_match('#^' . $path['path'] . '/?$#', $uri, $args))
			{
				/* Remove full string match. It isn't interesting. */
				array_shift($args);

				jaf()->modules->setActiveModule($path['module']);

				return call_user_func_array([jaf()->modules->getModule($path['module']), $path['handler']], $args);
			}
		}

		Error::httpError();
	}
}