<?php

namespace JAF;

class Database extends \PDO
{
	/**
	 * Database constructor.
	 * Sets up PDO with wanted parameters
	 */
	public function __construct()
	{
		$dsn = "mysql:host=" . config('database', 'host') .
			";port=" . config('database', 'port') . ";dbname=" . config('database', 'name');

		parent::__construct($dsn, config('database', 'username'), config('database', 'password'));

		$this->setAttribute(self::ATTR_DEFAULT_FETCH_MODE, self::FETCH_ASSOC);
	}
}