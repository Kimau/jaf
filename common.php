<?php

use \JAF\Error;

function lang($key, $module = null, $args = null)
{
	if (is_object(jaf()) && is_object(jaf()->language))
	{
		return jaf()->language->getString($key, $module, $args);
	}
	else
	{
		Error::warning("Attempt to use language key before being loaded: " . $key);
		return $key;
	}
}

/**
 * @return \JAF\Database Database object
 */
function db()
{
	if (is_object(jaf()) && is_object(jaf()->database))
	{
		return jaf()->database;
	}
	else
	{
		Error::fatal(lang('DB_NOT_LOADED'));
	}
}

function config($section, $key)
{
	if (is_object(jaf()) && is_object(jaf()->config))
	{
		return jaf()->config->getItem($section, $key);
	}
	else
	{
		Error::fatal(lang('CONFIG_NOT_LOADED'));
	}
}

/**
 * @param string $time
 * @param string|null $timezone
 * @return \DateTime
 */
function getDateTimeObj($time = 'now', $timezone = null)
{
	$timezone = (is_null($timezone)) ? config('datetime', 'server_timezone') : $timezone;
	$tz_obj = new \DateTimeZone($timezone);

	try
	{
		return new \DateTime($time, $tz_obj);
	} catch (\Exception $e)
	{
		Error::fatal($e->getMessage());
	}
}

function getCustomDateTimeStr($time = 'now', $timezone = null, $format = null)
{
	$dt_obj = getDateTimeObj($time, $timezone);
	$format = (is_null($format)) ? config('datetime', 'datetime_format') : $format;
	$timezone = (is_null($timezone)) ? config('datetime', 'default_display_timezone') : $timezone;
	$dt_obj->setTimezone(new \DateTimeZone($timezone));

	return $dt_obj->format($format);
}

function getMysqlDateTimeStr($time = 'now')
{
	return getCustomDateTimeStr($time, config('datetime', 'server_timezone'), 'Y-m-d H:i:s');
}

function getDateStr($time = 'now')
{
	return getCustomDateTimeStr($time, null, config('datetime', 'date_format'));
}

function getDateTimeStr($time = 'now')
{
	return getCustomDateTimeStr($time, null, config('datetime', 'datetime_format'));
}

function getTimeStr($time = 'now')
{
	return getCustomDateTimeStr($time, null, config('datetime', 'time_format'));
}


/**
 * Similar to vsprintf, but with support for associative arrays
 *
 * Thanks to "www dot wesley at gmail dot com" at php.net
 * Code was posted as a note on php.net and is as such published under Creative Commons Attribution
 * The code is in it's original form, with the exception of slight adaptation to the general code style.
 *
 * Source: https://php.net/manual/en/function.vsprintf.php#83883
 * CC: https://secure.php.net/manual/en/cc.license.php
 *
 * @param string $format
 * @param array $data
 * @return string
 */
function vnsprintf($format, array $data)
{
	preg_match_all('/ (?<!%) % ( (?: [[:alpha:]_-][[:alnum:]_-]* | ([-+])? [0-9]+ (?(2) (?:\.[0-9]+)? | \.[0-9]+ ) ) ) \$ [-+]? \'? .? -? [0-9]* (\.[0-9]+)? \w/x', $format, $match, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
	$offset = 0;
	$keys = array_keys($data);
	foreach ($match as &$value)
	{
		if (($key = array_search($value[1][0], $keys)) !== false || (is_numeric($value[1][0]) && ($key = array_search((int)$value[1][0], $keys)) !== false))
		{
			$len = strlen($value[1][0]);
			$format = substr_replace($format, 1 + $key, $offset + $value[1][1], $len);
			$offset -= $len - strlen($key);
		}
	}
	return vsprintf($format, $data);
}